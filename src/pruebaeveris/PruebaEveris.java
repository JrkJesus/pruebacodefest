/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebaeveris;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author jesus
 */
public class PruebaEveris {
    
    private HashMap<String, Integer>[] datosProcesados;
    
    public PruebaEveris()
    {
        datosProcesados = new HashMap[4];
        for (int i = 0; i < 4; i++) 
        {
            datosProcesados[i] = new HashMap<>();
        }
    }
    
    
    public void actualizar(String municipio, int trimestre, int empleo)
    {
        
        if(datosProcesados[trimestre].containsKey(municipio))
        {
            datosProcesados[trimestre].put(municipio, datosProcesados[trimestre].get(municipio) + empleo);
            //System.out.println("viejo");
        }
        else
        {
            datosProcesados[trimestre].put(municipio, empleo);
           // System.out.println(trimestre + ": " + datosProcesados[trimestre].get(municipio));
        }
    }
    
    public void showDiff()
    {
        String municipio[] = new String[4];
        int valor[] = {Integer.MIN_VALUE,Integer.MIN_VALUE,Integer.MIN_VALUE,Integer.MIN_VALUE};
        for (int i = 0; i < 4; i++) 
        {
            Iterator it = datosProcesados[i].entrySet().iterator();
            while (it.hasNext()) 
            {
                Map.Entry e = (Map.Entry)it.next();
                if( (int)e.getValue()>valor[i] )
                {
                    valor[i] = (int)e.getValue();
                    municipio[i] = (String)e.getKey();
                }
            }
        }
        for (int i = 0; i < 4; i++) 
        {
            System.out.println(municipio[i] + " - " + valor[i]);
        }
    }
    
    public void desempleo(String fichero_a_leer) 
        throws UnsupportedEncodingException, FileNotFoundException, IOException
    {

        FileInputStream fis =new FileInputStream(fichero_a_leer);
        InputStreamReader isr = new InputStreamReader(fis, "UTF8"); 
        BufferedReader br = new BufferedReader(isr);

        String linea=br.readLine();
        while(linea!=null){
            String[] dato = linea.split(";");
            if(dato[2].equals("1"))
                {
                    int trimestre = (Integer.parseInt(dato[0])-201401)/3,
                            tasaEmpleo =  Integer.parseInt(dato[10]) 
                                            + Integer.parseInt(dato[11]) 
                                            + Integer.parseInt(dato[13])
                                            + Integer.parseInt(dato[14]);
                    
                    actualizar(dato[7],trimestre, tasaEmpleo);
                }
            linea=br.readLine();
        }

        br.close();
        isr.close();
        fis.close();
    }
    
    public void empleo(String fichero_a_leer) 
        throws UnsupportedEncodingException, FileNotFoundException, IOException
    {

        FileInputStream fis =new FileInputStream(fichero_a_leer);
        InputStreamReader isr = new InputStreamReader(fis, "UTF8"); 
        BufferedReader br = new BufferedReader(isr);

        String linea=br.readLine();
        while(linea!=null){
            String[] dato = linea.split(";");
            if(dato[2].equals("1"))
                {
                    int trimestre = (Integer.parseInt(dato[0])-201401)/3 ,
                            tasaDesmpleo = Integer.parseInt(dato[9])
                                             + Integer.parseInt(dato[12]);
                    
                    actualizar(dato[7],trimestre, -tasaDesmpleo);
                }
            linea=br.readLine();
        }

        br.close();
        isr.close();
        fis.close();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            File dir = new File(".");
            PruebaEveris procesamiento = new PruebaEveris();
            procesamiento.empleo(dir.getCanonicalPath()+"\\Dtes_empleo_por_municipios_2014_csv.csv");
           // procesamiento.desempleo("C:\\Users\\jesus\\Downloads\\Paro_por_municipios_2014_csv.csv");
            procesamiento.desempleo(dir.getCanonicalPath()+"\\Paro_por_municipios_2014_csv.csv");
            procesamiento.showDiff();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PruebaEveris.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PruebaEveris.class.getName()).log(Level.SEVERE, null, ex);
        }
                
    }    
}
